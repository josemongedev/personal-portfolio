import { ArrowDownIcon, EmailIcon } from "@chakra-ui/icons";
import {
  Button,
  Center,
  Flex,
  Heading,
  HStack,
  Image,
  Link,
  useColorModeValue,
} from "@chakra-ui/react";
import Paragraph from "components/Paragraph/Paragraph";
import Section from "components/Section/Section";
import NextLink from "next/link";
import React from "react";
import { SectionProps } from "../Section/Section";

export interface IntroProps extends SectionProps {}

const IntroSection: React.FC<IntroProps> = ({ highlight, ...props }) => {
  const bgIntro = useColorModeValue("gray.100", "whiteAlpha.100");

  return (
    <Section {...props} pt={{ base: "10vh", lg: "0" }}>
      <Flex
        shadow={"dark-lg"}
        flexDirection={{ base: "column", lg: "row" }}
        paddingX={{ base: "0", xl: "10%" }}
        bg={bgIntro}
        border="1px"
        borderStyle={"solid"}
        borderColor={highlight}
        my={"2%"}
        mx={{ base: 0, xl: "15%" }}
      >
        <Flex
          flex={1}
          flexDirection={{ base: "column" }}
          justifyContent={{ base: "flex-end", md: "center" }}
          p={"5%"}
        >
          <Heading as="h1" size="3xl" mb={"10"} textAlign={"right"}>
            Hi, I&apos;m <br />
            <NextLink href={"/"}>
              <Link color={highlight}>
                <span style={{ whiteSpace: "nowrap" }}>Jose Monge</span>
              </Link>
            </NextLink>
          </Heading>
          <Heading as="h6" size="md" mb={"10"} textAlign={"right"}>
            Welcome to my portfolio site!
          </Heading>
          <br />
          <Paragraph>
            I&apos;m a <Link color={highlight}>digital developer</Link>
            &nbsp; from Costa Rica ready for the mission! Formerly, worked as
            embedded software engineer, but after 2020 decided to become a&nbsp;
            <Link color={highlight}>self-taught</Link> web developer.
          </Paragraph>
          <br />
          <HStack justify={"center"} m={"5%"}>
            {/* <Button
              as={Link}
              href="/resume/pdf/josemonge-23-march-2022.pdf"
              leftIcon={<ArrowDownIcon />}
              borderColor={highlight}
              color={highlight}
              variant={"outline"}
              download
            >
              Download CV
            </Button> */}
            <Button
              as={Link}
              href={"#contact"}
              leftIcon={<EmailIcon />}
              borderColor={highlight}
              color={highlight}
              variant={"outline"}
            >
              Contact me
            </Button>
          </HStack>
        </Flex>
        <Center flex={1} justifyContent={{ base: "center" }} p={"5%"}>
          <Image
            borderRadius={"base"}
            boxSize={{ base: "350px", borderRadius: "50%" }}
            fit="cover"
            src={"/images/profilecolor.jpg"}
            alt={"Jose Monge, Developer"}
          />
        </Center>
      </Flex>
    </Section>
  );
};

export default IntroSection;
