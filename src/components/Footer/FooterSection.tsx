import { Heading, HStack, Link, VStack } from "@chakra-ui/react";
import Section from "components/Section/Section";
import NextLink from "next/link";
import React from "react";
import { AiFillGithub, AiFillGitlab, AiFillLinkedin } from "react-icons/ai";
import { SectionProps } from "../Section/Section";
import { SiCodewars, SiHackerrank } from "react-icons/si";
interface FooterProps extends SectionProps {}

const FooterSection: React.FC<FooterProps> = ({ highlight, ...props }) => {
  return (
    <Section {...props}>
      <VStack>
        <Heading size={"md"}>
          You can also contact me on social media or see my work
        </Heading>
        <HStack>
          <Link color={highlight}>
            <NextLink href="https://gitlab.com/josemongedev">
              <AiFillGitlab size="3rem" />
            </NextLink>
          </Link>
          <Link color={highlight}>
            <NextLink href="https://www.linkedin.com/in/jose-monge-053453233/">
              <AiFillLinkedin size="3rem" />
            </NextLink>
          </Link>
          <Link color={highlight}>
            <NextLink href="https://github.com/josemongedev">
              <AiFillGithub size="3rem" />
            </NextLink>
          </Link>
          <Link color={highlight}>
            <NextLink href="https://www.codewars.com/users/josemon85">
              <SiCodewars size="3rem" />
            </NextLink>
          </Link>
          <Link color={highlight}>
            <NextLink href="https://www.hackerrank.com/josemon85">
              <SiHackerrank size="3rem" />
            </NextLink>
          </Link>
        </HStack>
        <Heading as="h6" size="xs">
          San José, Costa Rica. August, 2023.
        </Heading>
      </VStack>
    </Section>
  );
};

export default FooterSection;
