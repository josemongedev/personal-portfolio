import { MoonIcon, SunIcon } from "@chakra-ui/icons";
import {
  Flex,
  HStack,
  Icon,
  IconButton,
  Link,
  Square,
  Text,
  useColorMode,
  useColorModeValue,
} from "@chakra-ui/react";
import MobileMenu from "components/MobileMenu/MobileMenu";
import { useRouter } from "next/router";
import React, { PropsWithChildren, useState } from "react";
import { IoRocketSharp } from "react-icons/io5";
import { RiRocketFill } from "react-icons/ri";
import DesktopMenu from "../DesktopMenu/DesktopMenu";

const Left: React.FC<PropsWithChildren> = ({ children }) => (
  <HStack flex={1} ml={"2%"} justify={"flex-start"} align={"center"}>
    {children}
  </HStack>
);
const Right: React.FC<PropsWithChildren> = ({ children }) => (
  <HStack flex={1} justify={"flex-end"} align={"center"}>
    {children}
  </HStack>
);

type Props = {};
const Navbar: React.FC = (props: Props) => {
  const [rocketOn, setRocketOn] = useState<boolean>(false);
  const router = useRouter();
  const { toggleColorMode } = useColorMode();
  const colorValue = useColorModeValue("gray.300", "gray.900");
  const iconBackground = useColorModeValue("gray.700", "yellow.500");
  return (
    <Flex
      pos={"fixed"}
      minHeight={"50px"}
      height={"10vh"}
      background={colorValue}
      width={"100%"}
      paddingX={"10%"}
      zIndex={1}
    >
      <Link href="/" onClick={() => setRocketOn(true)}>
        <Square
          minHeight={"50px"}
          minWidth={"50px"}
          size="10vh"
          bg={iconBackground}
          color="white"
        >
          <Icon
            as={!rocketOn ? RiRocketFill : IoRocketSharp}
            w={"50%"}
            h={"50%"}
          />
        </Square>
      </Link>

      <Left>
        <Text>Jose Monge - Portfolio</Text>
      </Left>
      <Right>
        <DesktopMenu currentPath={router.pathname} />
        <IconButton
          aria-label="Toggle theme"
          bg={iconBackground}
          color={"white"}
          icon={useColorModeValue(<MoonIcon />, <SunIcon />)}
          onClick={toggleColorMode}
        />
        <MobileMenu />
      </Right>
    </Flex>
  );
};
{
}
export default Navbar;
