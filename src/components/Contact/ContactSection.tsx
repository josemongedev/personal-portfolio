import React from "react";
import Section from "components/Section/Section";
import { SectionProps } from "../Section/Section";
import {
  Box,
  Button,
  Flex,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  Input,
  Textarea,
} from "@chakra-ui/react";
import { EmailIcon } from "@chakra-ui/icons";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { HStack, useColorModeValue } from "@chakra-ui/react";
import emailjs from "emailjs-com";
import { init } from "@emailjs/browser";
init("user_uRZ4JBWKxUIHkLhurWhR0");

interface ContactProps extends SectionProps {}

const ContactSection: React.FC<ContactProps> = ({ highlight, ...props }) => {
  const bgForm = useColorModeValue("gray.100", "whiteAlpha.100");

  const sendEmail = (values: any) => {
    emailjs
      .send(
        "service_sro2ttm",
        "template_8op0ke3",
        values,
        "user_uRZ4JBWKxUIHkLhurWhR0"
      )
      .then(
        function (response) {
          window.alert(
            "Your message has been sent! I will contact you back as soon as possible."
          );
        },
        function (error) {
          window.alert("There was an error submitting your message");
        }
      );
  };

  const validationContactSchema = Yup.object().shape({
    name: Yup.string().required("Please provide a contact name"),
    email: Yup.string()
      .email("Invalid email address format")
      .required("A valid email address is required"),
    subject: Yup.string(),
    message: Yup.string().required("Please write the reason for contacting me"),
  });

  const handleSubmit = (values: any, actions: any) => {
    setTimeout(() => {
      sendEmail(values);
      actions.resetForm();
      actions.setSubmitting(false);
    }, 1000);
  };

  return (
    <Section id="contact" {...props} title={"Contact form"}>
      <Flex
        shadow={"dark-lg"}
        flexDirection={"column"}
        paddingX={{ base: "0", xl: "10%" }}
        bg={bgForm}
        border="1px"
        borderStyle={"solid"}
        borderColor={highlight}
        p={"5%"}
      >
        <Heading as={"h6"} size={"md"} mb={"2%"}>
          Send me a message, I will answer back shortly
        </Heading>
        <Formik
          initialValues={{ name: "", email: "", subject: "", message: "" }}
          onSubmit={handleSubmit}
          validationSchema={validationContactSchema}
        >
          {(props) => (
            <Form>
              <HStack h={"10vh"} alignItems={"flex-start"}>
                <Field name="name">
                  {({ field, form }: any) => (
                    <FormControl
                      isInvalid={form.errors.name && form.touched.name}
                    >
                      <FormLabel htmlFor="name">Name</FormLabel>
                      <Input {...field} id="name" placeholder="name" />
                      <FormErrorMessage>{form.errors.name}</FormErrorMessage>
                    </FormControl>
                  )}
                </Field>
                <Field name="email">
                  {({ field, form }: any) => (
                    <FormControl
                      isInvalid={form.errors.email && form.touched.email}
                    >
                      <FormLabel htmlFor="email">Email</FormLabel>
                      <Input {...field} id="email" placeholder="email" />
                      <FormErrorMessage>{form.errors.email}</FormErrorMessage>
                    </FormControl>
                  )}
                </Field>
              </HStack>
              <Box h={"10vh"} alignItems={"flex-start"}>
                <Field name="subject">
                  {({ field, form }: any) => (
                    <FormControl
                      isInvalid={form.errors.subject && form.touched.subject}
                    >
                      <FormLabel htmlFor="subject">Subject</FormLabel>
                      <Input {...field} id="subject" placeholder="subject" />
                      <FormErrorMessage>{form.errors.subject}</FormErrorMessage>
                    </FormControl>
                  )}
                </Field>
              </Box>
              <Field name="message">
                {({ field, form }: any) => (
                  <FormControl
                    isInvalid={form.errors.message && form.touched.message}
                  >
                    <FormLabel htmlFor="message">Message</FormLabel>
                    <Textarea
                      rows={4}
                      {...field}
                      id="message"
                      placeholder="message"
                    />
                    <FormErrorMessage>{form.errors.message}</FormErrorMessage>
                  </FormControl>
                )}
              </Field>

              <Button
                leftIcon={<EmailIcon />}
                borderColor={highlight}
                color={highlight}
                variant={"outline"}
                mt={4}
                colorScheme="teal"
                isLoading={props.isSubmitting}
                type="submit"
              >
                Send message
              </Button>
            </Form>
          )}
        </Formik>
      </Flex>
    </Section>
  );
};

export default ContactSection;
