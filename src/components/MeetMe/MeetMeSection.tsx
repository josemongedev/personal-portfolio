import { Center, Flex, Image, Link, useColorModeValue } from "@chakra-ui/react";
import Paragraph from "components/Paragraph/Paragraph";
import Section, { SectionProps } from "components/Section/Section";
import React from "react";
import { experience } from "../Experience/cv";

export interface MeetMeProps extends SectionProps {}
const MeetMeSection: React.FC<MeetMeProps> = ({ highlight, ...props }) => {
  const bgForm = useColorModeValue("gray.200", "whiteAlpha.100");

  return (
    <Section id="about" {...props} title={"Meet the developer"}>
      <Flex
        shadow={"dark-lg"}
        flexDirection={{ base: "column", lg: "row" }}
        paddingX={{ base: "0", xl: "10%" }}
        bg={bgForm}
        border="1px"
        borderStyle={"solid"}
        borderColor={highlight}
        p={"5%"}
      >
        <Center
          flex={1}
          px={"10%"}
          mb={{ base: "10%", lg: "0" }}
          flexDirection={"column"}
        >
          <Paragraph>
            Although I initially studied electronics/electrical engineering and
            worked on firmware development on embedded devices using C/C++ and
            other scripting languages, I&apos;ve become very comfortable with
            full stack web development technologies since I decided to broaden
            my professional skills towards software engineering, mostly with the{" "}
            <strong>MERN stack</strong> and <strong>Next.js</strong> framework.
            I have some mild experience with Python(Django and Flask) and PHP
            (Laravel) for personal projects as well. I like ocassionally
            practicing with short coding challenges on&nbsp;
            <Link
              href="https://www.hackerrank.com/josemon85"
              fontWeight={"bold"}
            >
              Hackerrank
            </Link>
            ,&nbsp;
            <Link
              href="https://www.codewars.com/users/josemon85"
              fontWeight={"bold"}
            >
              Codewars
            </Link>
            &nbsp;and&nbsp;
            <Link
              href="https://www.frontendmentor.io/profile/josemongedev"
              fontWeight={"bold"}
            >
              Frontend Mentor
            </Link>
            &nbsp;to keep these skills sharp. If you would prefer to check my
            personal projects or frontend templates I&apos;ve built you can see
            them on&nbsp;
            <Link href="https://gitlab.com/josemongedev" fontWeight={"bold"}>
              Gitlab
            </Link>
            &nbsp;or&nbsp;
            <Link href="https://github.com/josemongedev" fontWeight={"bold"}>
              Github.
            </Link>
            &nbsp;So my working experience is mostly on the embedded development
            side, but I offer these projects and practices as evidence of some
            of my skills as project based experience.
          </Paragraph>
        </Center>
        <Center flex={1} justifyContent={{ base: "center" }}>
          <Image
            borderRadius={"50%"}
            boxSize={{ base: "350px" }}
            fit="cover"
            src={"/images/meetme.jpg"}
            alt={"Jose Monge, Developer"}
          />
        </Center>
      </Flex>
    </Section>
  );
};

export default MeetMeSection;
