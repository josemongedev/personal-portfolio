import React, { PropsWithChildren } from "react";
import Navbar from "../Navbar/Navbar";
import Fonts from "../Fonts/Fonts";

type Props = {};

const Layout: React.FC<PropsWithChildren<Props>> = ({ children }) => {
  return (
    <>
      <Fonts />
      <Navbar />
      {children}
    </>
  );
};

export default Layout;
