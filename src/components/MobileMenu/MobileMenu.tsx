import { HamburgerIcon } from "@chakra-ui/icons";
import {
  Box,
  Menu,
  MenuButton,
  IconButton,
  MenuList,
  MenuItem,
  Link,
} from "@chakra-ui/react";
import NextLink from "next/link";
import React from "react";

type Props = {};

const MobileMenu = (props: Props) => {
  return (
    <Box display={{ base: "inline-block", md: "none" }}>
      <Menu>
        <MenuButton
          as={IconButton}
          icon={<HamburgerIcon />}
          variant="outline"
          aria-label="Options"
        />
        <MenuList>
          <NextLink href="#about" passHref>
            <MenuItem as={Link}>About</MenuItem>
          </NextLink>
          <NextLink href="#projects" passHref>
            <MenuItem as={Link}>Projects</MenuItem>
          </NextLink>
          {/* <NextLink href="/posts" passHref>
            <MenuItem as={Link}>Posts</MenuItem>
          </NextLink> */}
          <MenuItem
            as={Link}
            _target="_blank"
            href="https://gitlab.com/josemongedev"
          >
            Sources
          </MenuItem>
        </MenuList>
      </Menu>
    </Box>
  );
};

export default MobileMenu;
