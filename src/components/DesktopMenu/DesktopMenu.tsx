import { HStack, Link, LinkProps } from "@chakra-ui/react";
import NextLink from "next/link";
import React from "react";
import { IoLogoGitlab } from "react-icons/io5";

interface StyledLinkProps extends LinkProps {
  href: string;
  path: string;
  active?: string;
  inactiveColor?: string;
  _target?: string;
}
const StyledLink: React.FC<StyledLinkProps> = ({
  href,
  active,
  inactiveColor,
  _target,
  path,
  children,
  ...props
}) => (
  <Link
    href={href}
    p={2}
    bg={active ? "grassTeal" : undefined}
    color={active ? "#202023" : inactiveColor}
    target={_target}
    {...props}
  >
    {children}
  </Link>
);

interface DesktopMenuProps {
  currentPath: string;
}
const DesktopMenu = ({ currentPath: path }: DesktopMenuProps) => {
  return (
    <HStack display={{ base: "none", md: "flex" }}>
      <StyledLink href="#projects" path={path}>
        Projects
      </StyledLink>
      {/* <StyledLink href="/posts" path={path}>
        Posts
      </StyledLink> */}
      <StyledLink
        _target="_blank"
        href="https://gitlab.com/josemongedev"
        path={path}
        display={"inline-flex"}
        alignItems="center"
        style={{ gap: 4 }}
        pl={2}
      >
        <IoLogoGitlab />
        Source
      </StyledLink>
    </HStack>
  );
};

export default DesktopMenu;
