import { Text } from "@chakra-ui/react";
import React, { PropsWithChildren } from "react";

type Props = {};

const Paragraph: React.FC<PropsWithChildren<Props>> = ({ children }) => (
  <Text fontSize={"xl"} align={"justify"}>
    {children}
  </Text>
);

export default Paragraph;
