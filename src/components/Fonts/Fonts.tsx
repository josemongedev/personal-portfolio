import { Global } from "@emotion/react";

const Fonts = () => {
  return (
    <Global
      styles={`
          @font-face {
            font-family: 'M Plus Rounded 1c';
            font-style: normal;
            font-weight: 300;
            font-display: swap;
            src: url('./fonts/MPLUSRounded1c-Bold.ttf');
          }
          @font-face {
            font-family: 'M Plus Rounded 1c';
            font-style: normal;
            font-weight: 700;
            font-display: swap;
            src: url('./fonts/MPLUSRounded1c-Light.ttf');
          }
          `}
    />
  );
};

export default Fonts;
