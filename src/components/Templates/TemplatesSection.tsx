import {
  Button,
  Center,
  Flex,
  Grid,
  GridItem,
  HStack,
  Image,
  Link,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import Paragraph from "components/Paragraph/Paragraph";
import Section from "components/Section/Section";
import NextLink from "next/link";
import React from "react";
import { RiPlayCircleLine } from "react-icons/ri";
import { SiGithub } from "react-icons/si";
import { SectionProps } from "../Section/Section";
import { templates } from "./templates";

export interface TemplateProps extends SectionProps {}

const TemplatesSection: React.FC<TemplateProps> = ({ highlight, ...props }) => {
  const detailColorValue = useColorModeValue("gray.200", "yellow.500");
  const bgValue = useColorModeValue("gray.100", "gray.800");
  const fontValue = useColorModeValue("gray.700", "gray.200");
  return (
    <Section
      id="templates"
      {...props}
      title={"Frontend Mentor sample templates (static)"}
    >
      <Grid
        templateColumns={{
          base: "repeat(1,1fr)",
          md: "repeat(2, 1fr)",
          lg: "repeat(4, 1fr)",
        }}
        templateRows={{
          base: "repeat(auto-fill,1fr)",
          md: "repeat(auto-fill,1fr)",
        }}
        gap={6}
        bg={"whiteAlpha.100"}
        width={"100%"}
        height={"100%"}
        p={"5%"}
      >
        {templates.map((template) => (
          <GridItem
            shadow={"dark-lg"}
            key={template.name}
            bg={bgValue}
            borderBottomWidth={"1px"}
            borderBottomStyle={"solid"}
            borderBottomColor={highlight}
          >
            <Center flexDirection={"column"} h={"100%"} w={"100%"}>
              <Image
                flex={1}
                _hover={{
                  filter: "none",
                }}
                sx={{
                  filter: " grayscale(100%)",
                }}
                h={"auto"}
                w={"100%"}
                src={template.img}
                alt={template.name}
              />
              <Flex
                flex={1}
                flexDirection={{ base: "column" }}
                justifyContent={"flex-start"}
                p={"5%"}
              >
                <Text color={highlight} size="xl" textAlign={"left"}>
                  {template.type}
                </Text>
                <Text color={fontValue} fontSize={"2xl"} textAlign={"left"}>
                  {template.name}
                </Text>

                <Paragraph>{template.desc}</Paragraph>
                <br />
              </Flex>
              <HStack justify={"center"} mb={"10%"}>
                <NextLink href={template.live} passHref>
                  <Button
                    target="_blank"
                    as={Link}
                    leftIcon={<RiPlayCircleLine />}
                    borderColor={highlight}
                    color={highlight}
                    variant={"outline"}
                  >
                    Open live
                  </Button>
                </NextLink>
                <NextLink href={template.src} passHref>
                  <Button
                    target="_blank"
                    as={Link}
                    leftIcon={<SiGithub />}
                    borderColor={highlight}
                    color={highlight}
                    variant={"outline"}
                  >
                    View Source
                  </Button>
                </NextLink>
              </HStack>
            </Center>
          </GridItem>
        ))}
      </Grid>
    </Section>
  );
};

export default TemplatesSection;
