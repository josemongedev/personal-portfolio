export const templates = [
  {
    name: "Pomodoro",
    type: "App",
    img: "/images/pomodorotimer.png",
    live: "https://josemongedev.github.io/pomodoro-app/",
    src: "https://github.com/josemongedev/pomodoro-app",
    desc: "This app is a pomodoro timer that runs on different intervals and sounds an alarm after running out of time.",
  },
  {
    name: "Landing page",
    type: "Website",
    img: "/images/sunnyside.png",
    live: "https://josemongedev.github.io/sunnyside-agency-landing-page/",
    src: "https://github.com/josemongedev/sunnyside-agency-landing-page",
    desc: "Sunnyside landing page is a colorful page made using Astro that shows a nice use of grid and flexbox in CSS.",
  },
  {
    name: "Planets",
    type: "Multi-page website",
    img: "/images/planets.png",
    live: "https://josemongedev.github.io/planets-fact-site/",
    src: "https://github.com/josemongedev/planets-fact-site",
    desc: "Multi-page static site using Astro and several SVG graphics.",
  },
  {
    name: "Base Apparel",
    type: "Widget",
    img: "/images/baseapparel.png",
    live: "https://josemongedev.github.io/base-apparel-coming-soon/",
    src: "https://github.com/josemongedev/base-apparel-coming-soon",
    desc: "Static widget showing the use of SVG and masking to achieve nice colors and textures.",
  },
];
