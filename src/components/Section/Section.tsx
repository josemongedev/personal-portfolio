import {
  Box,
  BoxProps,
  Heading,
  Center,
  useColorModeValue,
} from "@chakra-ui/react";
import React from "react";

export interface SectionProps extends BoxProps {
  title?: string;
  backgroundColor?: string;
  highlight?: string;
}
const Section: React.FC<SectionProps> = ({
  title,
  backgroundColor,
  highlight,
  children,
  ...props
}) => {
  const sectionTitleColor = useColorModeValue("gray.600", "gray.100");

  return (
    <Box
      backgroundColor={backgroundColor}
      as="section"
      display={"flex"}
      height={{ base: "100%", sm: "100%" }}
      width={"100%"}
      flexWrap={"wrap"}
      align={"center"}
      justify={"center"}
      {...props}
    >
      <Box p={"5%"} w={"100%"} h={"100%"}>
        {title && (
          <Heading mb={"10"} textColor={sectionTitleColor}>
            {title}
          </Heading>
        )}
        <Center>{children}</Center>
      </Box>
    </Box>
  );
};

export default Section;
