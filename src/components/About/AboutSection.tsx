import Section, { SectionProps } from "components/Section/Section";
import React from "react";
import { Box, Flex, Image, Center, useColorModeValue } from "@chakra-ui/react";
import Paragraph from "components/Paragraph/Paragraph";

interface AboutProps extends SectionProps {}

const AboutSection: React.FC<AboutProps> = ({ highlight, ...props }) => {
  const bgForm = useColorModeValue("gray.200", "whiteAlpha.100");

  return (
    <Section id="about" {...props} title={"About Jose"}>
      <Flex
        shadow={"dark-lg"}
        flexDirection={{ base: "column", lg: "row" }}
        paddingX={{ base: "0", xl: "10%" }}
        bg={bgForm}
        border="1px"
        borderStyle={"solid"}
        borderColor={highlight}
        p={"5%"}
      >
        <Center
          flex={1}
          px={"10%"}
          mb={{ base: "10%", lg: "0" }}
          flexDirection={"column"}
        >
          <Paragraph>
            Ever since he was a teenager when he decided to learn to code on
            Visual Basic 6.0 with help from his uncle, he has considered
            software development, not only as a skill, but as his passion. He
            has an interest on self-learning and self-development that takes
            very seriously, and is constantly looking out for new interesting
            technologies.
          </Paragraph>
          <Paragraph>
            On his free time Jose plays the guitar, listens either LoFi beats or
            trance music, walking around the city for some cardio and likes
            learning new recipes for his cooking skills. He likes reading about
            new Physics and scientific discoveries and making experiments with
            his Arduino. Finally, from time to time he likes playing on the
            computer.
          </Paragraph>
        </Center>
        <Center flex={1} justifyContent={{ base: "center" }}>
          <Image
            borderRadius={"base"}
            boxSize={{ base: "350px" }}
            fit="cover"
            src={"/images/profileblackwhite.jpg"}
            alt={"Jose Monge, Developer"}
          />
        </Center>
      </Flex>
    </Section>
  );
};

export default AboutSection;
