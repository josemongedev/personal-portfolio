import Section from "components/Section/Section";
import React from "react";
import {
  Box,
  Grid,
  GridItem,
  Center,
  Heading,
  useColorModeValue,
} from "@chakra-ui/react";
import { SectionProps } from "../Section/Section";

import { Icon } from "@chakra-ui/react";
import { skills } from "./skills";

export interface SkillsProps extends SectionProps {}
const SkillsSection: React.FC<SkillsProps> = ({ highlight, ...props }) => {
  const detailColorValue = useColorModeValue("gray.200", "yellow.500");
  const bgValue = useColorModeValue("gray.100", "gray.800");
  return (
    <Section {...props} title={"My Explored Skills"}>
      <Grid
        templateColumns={{ base: "repeat(1,1fr)", md: "repeat(5, 1fr)" }}
        templateRows={{ base: "repeat(auto-fill,1fr)", md: "repeat(4,1fr)" }}
        gap={6}
        bg={"whiteAlpha.100"}
        width={"100%"}
        height={"auto"}
        p={"5%"}
      >
        <GridItem>
          <Center h={"100%"} w={"100%"} alignItems={"center"}>
            <Heading as="h1" size="2xl" mb={"10"} color={highlight}>
              Tech Stack
            </Heading>
          </Center>
        </GridItem>
        {skills.map((skill) => (
          <GridItem
            shadow={"dark-lg"}
            key={skill.name}
            bg={bgValue}
            border="1px"
            borderStyle={"solid"}
            borderColor={highlight}
          >
            <Center flexDirection={"column"} h={"100%"} w={"100%"}>
              <Icon color={highlight} boxSize={"50%"} as={skill.logo} />
              {skill.name}
            </Center>
          </GridItem>
        ))}
      </Grid>
    </Section>
  );
};

export default SkillsSection;
