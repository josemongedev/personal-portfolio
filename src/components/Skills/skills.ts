import {
  SiTypescript,
  SiHtml5,
  SiCss3,
  SiJavascript,
  SiNextdotjs,
  SiChakraui,
  SiStyledcomponents,
  SiVuedotjs,
  SiVuetify,
  SiExpress,
  SiNodedotjs,
  SiNuxtdotjs,
  SiBulma,
  SiMongodb,
  SiPostgresql,
  SiC,
  SiCplusplus,
  SiPython,
  SiReact,
  SiRedux,
  SiMaterialdesign,
} from "react-icons/si";
export const skills = [
  {
    name: "HTML5",
    logo: SiHtml5,
  },
  {
    name: "CSS3",
    logo: SiCss3,
  },
  {
    name: "ES6",
    logo: SiJavascript,
  },
  {
    name: "Typescript",
    logo: SiTypescript,
  },
  {
    name: "React",
    logo: SiReact,
  },
  {
    name: "Redux",
    logo: SiRedux,
  },
  {
    name: "Next.js",
    logo: SiNextdotjs,
  },
  {
    name: "Material UI",
    logo: SiMaterialdesign,
  },
  {
    name: "Chakra UI",
    logo: SiChakraui,
  },
  {
    name: "Styled Components",
    logo: SiStyledcomponents,
  },

  {
    name: "Express",
    logo: SiExpress,
  },
  {
    name: "Node.js",
    logo: SiNodedotjs,
  },
  {
    name: "MongoDB",
    logo: SiMongodb,
  },
  {
    name: "PostgresQL",
    logo: SiPostgresql,
  },
  {
    name: "Vue 3",
    logo: SiVuedotjs,
  },
  {
    name: "Nuxt 3",
    logo: SiNuxtdotjs,
  },
  {
    name: "VueX",
    logo: SiVuedotjs,
  },
  {
    name: "Vuetify",
    logo: SiVuetify,
  },
  {
    name: "Bulma",
    logo: SiBulma,
  },
  {
    name: "C",
    logo: SiC,
  },
  {
    name: "C++",
    logo: SiCplusplus,
  },
  {
    name: "Python",
    logo: SiPython,
  },
];
