interface cvItem {
  year: string;
  activity: string;
  description: string;
}

export const experience: cvItem[] = [
  {
    year: "2004",
    activity:
      "Learned basic programming concepts, data structures and algorithms and database design. Pascal, C, VisualFox, SQL.",
    description: "Don Bosco Technical Highschool, Informatics.",
  },
  {
    year: "2010",
    activity:
      "Studied topics like networking, finite automata design, computer architectures, assembly language and linux Programming. Digital signal processing algorithms",
    description: "University of Costa Rica, Bsc. Electrical Eng.",
  },
  {
    year: "2010-2011",
    activity:
      "Worked as QA testing engineer with former Mobility team (Wireless). Managed virtual machine testbeds running test scripts. Developed scripts using tools like PHP, Bash, Git, Wireshark (network packets).",
    description: "Hewlett-Packard, HP Networking",
  },
  {
    year: "2011-2013",
    activity:
      "Worked at as developer for networking devices (Digital Signal Processing). Filter design, simulation, optimization techniques. Matlab, Python, C/C++ Development. ",
    description: "InBand Software, Firmware Developer.",
  },
  {
    year: "2014-2018",
    activity:
      "Studied advanced signal processing techniques, mobile network systems, applied information theory, RF systems. Pursued with own funds. Not concluded due to financial restrictions.",
    description: "RWTH Aachen, Msc. Comms Eng.",
  },
  {
    year: " 2021",
    activity:
      "Started learning with project based experience, mostly on frontend. Some experience with backend development.",
    description: "Self-taught Web Development",
  },
];
