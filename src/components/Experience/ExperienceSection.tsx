import {
  Box,
  Center,
  Circle,
  Heading,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import Section from "components/Section/Section";
import React from "react";
import { SectionProps } from "../Section/Section";
import { experience } from "./cv";

interface ExperienceProps extends SectionProps {}

const ExperienceSection: React.FC<ExperienceProps> = ({
  highlight,
  ...props
}) => {
  const panelColor = useColorModeValue("gray.100", "gray.800");
  const textColor = useColorModeValue("gray.700", "gray.200");
  const titleColor = useColorModeValue("gray.900", "gray.400");
  return (
    <Section {...props} title={"Education/Work Timeline"}>
      <Box
        display={"flex"}
        flexDirection={"column"}
        paddingX={{ base: "0", xl: "10%" }}
        bg={"whiteAlpha.100"}
        width={"auto"}
        justifyContent={"center"}
        alignItems={"center"}
        p={"5%"}
      >
        {experience.map((cvItem, index) => {
          const marginL = index % 2 === 0 ? "calc(50% + 20px)" : "0";
          const marginR = index % 2 !== 0 ? "calc(50% + 20px)" : "0";
          return (
            <Box
              display={"flex"}
              justifyContent={"center"}
              position={"relative"}
              key={cvItem.description}
              width={"100%"}
            >
              <Circle
                position={"absolute"}
                size={"10px"}
                bg={highlight}
                left={{ base: "0", md: "calc(50% - 4px)" }}
              />
              <Box
                position="absolute"
                width="2px"
                height="100%"
                backgroundColor={highlight}
                left={{ base: "4px", md: "50%" }}
              />

              <Heading
                position="absolute"
                left={{ base: "20px", md: "calc(50% + 10px)" }}
                color={highlight}
                as="h6"
                size="xs"
              >
                {cvItem.year}
              </Heading>
              <Center
                shadow={"dark-lg"}
                mt={{ base: "5%", md: "1.5%" }}
                mb={{ base: "7%", md: "2%" }}
                display={"flex"}
                flexDir={"column"}
                ml={{ base: "20px", md: marginL }}
                mr={{ base: "auto", md: marginR }}
                width={{ base: "100%", md: "80%" }}
                height={{ base: "300px", lg: "200px" }}
                bg={panelColor}
                p={"5%"}
                borderColor={highlight}
                borderStyle={"solid"}
                borderWidth={"1px"}
              >
                <Text
                  textAlign={"center"}
                  fontWeight={"700"}
                  fontSize={"xl"}
                  color={titleColor}
                >
                  {cvItem.description}
                </Text>
                <br />
                <Text textAlign={"justify"} color={textColor}>
                  {cvItem.activity}
                </Text>
              </Center>
            </Box>
          );
        })}
        <Box
          display={"flex"}
          justifyContent={"center"}
          position={"relative"}
          width={"100%"}
        >
          <Circle
            position={"absolute"}
            size={"10px"}
            bg={highlight}
            left={{ base: "0", md: "calc(50% - 4px)" }}
          />
          <Heading
            position="absolute"
            left={{ base: "20px", md: "calc(50% + 10px)" }}
            color={highlight}
            as="h6"
            size="xs"
          >
            {"2022"}
          </Heading>
        </Box>
      </Box>
    </Section>
  );
};

export default ExperienceSection;
