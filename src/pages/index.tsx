import { Box, useColorModeValue } from "@chakra-ui/react";
import ContactSection from "components/Contact/ContactSection";
import ExperienceSection from "components/Experience/ExperienceSection";
import FooterSection from "components/Footer/FooterSection";
import ProjectsSection from "components/Projects/ProjectsSection";
import type { NextPage } from "next";
import AboutSection from "components/About/AboutSection";
import IntroSection from "components/Intro/IntroSection";
import SkillsSection from "components/Skills/SkillsSection";
import MeetMeSection from "components/MeetMe/MeetMeSection";
import TemplatesSection from "components/Templates/TemplatesSection";

const Home: NextPage = () => {
  const colorValue = useColorModeValue("gray.300", "gray.900");
  const altColorValue = useColorModeValue("gray.200", "gray.800");
  const footerColorValue = useColorModeValue("gray.100", "gray.700");
  const highlight = useColorModeValue("gray.600", "yellow.500");

  return (
    <Box as={"main"}>
      <IntroSection backgroundColor={colorValue} highlight={highlight} />
      <MeetMeSection backgroundColor={altColorValue} highlight={highlight} />
      <ProjectsSection backgroundColor={colorValue} highlight={highlight} />
      <TemplatesSection backgroundColor={altColorValue} highlight={highlight} />
      <SkillsSection backgroundColor={colorValue} highlight={highlight} />
      {/* <ExperienceSection
        backgroundColor={altColorValue}
        highlight={highlight}
      /> */}
      <AboutSection backgroundColor={colorValue} highlight={highlight} />
      <ContactSection backgroundColor={altColorValue} highlight={highlight} />
      <FooterSection backgroundColor={footerColorValue} highlight={highlight} />
    </Box>
  );
};

export default Home;
