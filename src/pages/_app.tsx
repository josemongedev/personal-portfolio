import "../styles/globals.css";
import type { AppProps } from "next/app";
import { ChakraProvider, CSSReset, theme } from "@chakra-ui/react";
import Layout from "components/Layout/Layout";
import { Analytics } from "@vercel/analytics/react";

function App({ Component, pageProps }: AppProps) {
  return (
    <ChakraProvider theme={theme}>
      <CSSReset />
      <Layout>
        <Component {...pageProps} />
        <Analytics />
      </Layout>
    </ChakraProvider>
  );
}

export default App;
